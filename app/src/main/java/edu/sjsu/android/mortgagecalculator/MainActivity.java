package edu.sjsu.android.mortgagecalculator;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText et_amount;
    CheckBox chk_taxes;
    RadioGroup radioGroup;
    RadioButton rb_loan_term_15;
    RadioButton rb_loan_term_20;
    RadioButton rb_loan_term_30;
    SeekBar sb_interest_rate;
    Button btn_calculate;
    TextView tv_monthly_payment;
    TextView tv_interestRate;
    float seekbarValue = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_amount = findViewById(R.id.et_amount);
        chk_taxes = findViewById(R.id.chk_taxes);
        sb_interest_rate = findViewById(R.id.sb_interest_rate);
        btn_calculate = findViewById(R.id.btn_calculate);
        radioGroup = findViewById(R.id.radioGroup);
        rb_loan_term_15 = findViewById(R.id.rb_loan_term_15);
        rb_loan_term_20 = findViewById(R.id.rb_loan_term_20);
        rb_loan_term_30 = findViewById(R.id.rb_loan_term_30);
        tv_monthly_payment = findViewById(R.id.tv_monthly_payment);
        tv_interestRate = findViewById(R.id.tv_interestRate);

        sb_interest_rate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {

                seekbarValue = progressValue;
                tv_interestRate.setText(String.valueOf(progressValue));
                Toast.makeText(getApplicationContext(), progressValue + "", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

//                Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
//                seekbarValue = progress;
//                Toast.makeText(getApplicationContext(), "Stopped tracking seekbar at " + "/" + seekBar.getMax(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onClick(View view) {
        if(et_amount.getText().length() == 0){
            Toast.makeText(this, "Please enter a valid number",Toast.LENGTH_LONG).show();
            return;
        }
        float amountBorrowed = Float.parseFloat(et_amount.getText().toString());
//        float seekbarValue = (float) sb_interest_rate.getProgress();
        float monthlyInterest = seekbarValue / 100;
        if (rb_loan_term_15.isChecked()) {
            double loan_term = 15;
            double taxes_insurance = 0;
            if (chk_taxes.isChecked()) {
                taxes_insurance = (amountBorrowed * 0.1) / 100;
                double M =calculateMonthlyPayment(amountBorrowed,monthlyInterest,loan_term,taxes_insurance);
                tv_monthly_payment.setText(String.valueOf(M));
            }else {
                double M = calculateMonthlyPayment(amountBorrowed, monthlyInterest, loan_term, 0);
                tv_monthly_payment.setText(String.valueOf(M));
            }
        } else if (rb_loan_term_20.isChecked()) {
            double loan_term = 20;
            double taxes_insurance = 0;
            if (chk_taxes.isChecked()) {
                taxes_insurance = (amountBorrowed * 0.1) / 100;
                double M =calculateMonthlyPayment(amountBorrowed,monthlyInterest,loan_term,taxes_insurance);
                tv_monthly_payment.setText(String.valueOf(M));
            }else {
                double M = calculateMonthlyPayment(amountBorrowed, monthlyInterest, loan_term, 0);
                tv_monthly_payment.setText(String.valueOf(M));
            }
        } else {
            double loan_term = 30;
            double taxes_insurance = 0;
            if (chk_taxes.isChecked()) {
                taxes_insurance = (amountBorrowed * 0.1) / 100;
                double M =calculateMonthlyPayment(amountBorrowed,monthlyInterest,loan_term,taxes_insurance);
                tv_monthly_payment.setText(String.valueOf(M));
            }else {
                double M = calculateMonthlyPayment(amountBorrowed, monthlyInterest, loan_term, 0);
                tv_monthly_payment.setText(String.valueOf(M));
            }

            }

    }

    private double calculateMonthlyPayment(float amountBorrowed, float monthlyInterest, double loan_term, double taxes_insurance){
        double c = (double)(Math.pow((1 + monthlyInterest), -loan_term));
        double MonthlyPayment = (amountBorrowed * (monthlyInterest / (1-c))) + taxes_insurance;
        return MonthlyPayment;

    }
}
